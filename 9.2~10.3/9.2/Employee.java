package firm;

public class Employee extends StaffMember{
    protected String socialSecurityNumber;
    protected double payRate;

    public Employee (String eName, String eAddress, String ePhone,int eGrade,
                     String socSecNumber, double rate)
    {
        super (eName, eAddress, ePhone, eGrade);

        socialSecurityNumber = socSecNumber;
        payRate = rate;
    }

    public String toString()
    {
        String result = super.toString();

        result += "\nSocial Security Number: " + socialSecurityNumber;

        return result;
    }

    public double pay()
    {
        return payRate;
    }

    @Override
    public void holiday() {
        int op = grade;
        switch (op) {
            case 1:
                System.out.println("Holiday: 10");
                break;
            case 2:
                System.out.println("Holiday: 9");
                break;
            case 3:
                System.out.println("Holiday: 8");
                break;
            case 4:
                System.out.println("Holiday: 7");
                break;
            default:
                break;
        }
    }
}
