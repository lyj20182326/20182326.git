package firm;

public class Executive extends Employee{
    private double bonus;

    public Executive (String eName, String eAddress, String ePhone,int eGrade,
                      String socSecNumber, double rate)
    {
        super (eName, eAddress, ePhone, eGrade, socSecNumber, rate);

        bonus = 0;
    }

    public void awardBonus (double execBonus)
    {
        bonus = execBonus;
    }

    public double pay()
    {
        double payment = super.pay() + bonus;

        bonus = 0;

        return payment;
    }
}
