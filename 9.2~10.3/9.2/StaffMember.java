package firm;

abstract public class StaffMember
    {
        protected String name;
        protected String address;
        protected String phone;
        protected int grade;

        public StaffMember (String eName, String eAddress, String ePhone,int eGrade)
        {
            name = eName;
            address = eAddress;
            phone = ePhone;
            grade = eGrade;
        }

        public String toString()
        {
            String result = "Name: " + name + "\n";

            result += "Address: " + address + "\n";
            result += "Phone: " + phone + "\n";
            result += "Grade: " + grade;

            return result;
        }

        public abstract double pay();
        public abstract void holiday();
}
