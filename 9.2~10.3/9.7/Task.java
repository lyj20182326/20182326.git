package thing;

public class Task implements Priority{
    private int priority;
    private String work;

    public String getWork() {
        return work;
    }

    public void setJob(String work) {
        this.work = work;
    }

    public Task(int priority,String work) {
        this.priority = priority;
        this.work = work;
    }

    @Override
    public void setPriority() {
        this.priority = priority;
    }

    @Override
    public int getPriority() {
        return priority;
    }

    @Override
    public String toString() {
        return "Task{" +
                "priority = " + priority +
                ", job ='" + work +
                '}';
    }
}
