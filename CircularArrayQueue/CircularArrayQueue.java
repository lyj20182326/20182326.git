package javafoundations;

import java.util.Collection;
import java.util.Queue;
import java.util.Iterator;

public class CircularArrayQueue<T> implements Queue<T> {
    private final int DEFAULT_CAPACTTY = 10;
    private int front,rear,count;
    private T[] queue;
    public CircularArrayQueue()
    {
        front = rear = count = 0;
        queue = (T[])(new Object[DEFAULT_CAPACTTY]);
    }
    public void enqueue (T element)
    {
        if(count == queue.length)
            expandCapacity();

        queue[rear] = element;
        rear = (rear+1) % queue.length;
        count++;
    }
    public void expandCapacity()
    {
        T[] larger = (T[])(new Object[queue.length*2]);
        for (int index=0; index < count; index++)
            larger[index] = queue[(front+index) % queue.length];

        front = 0;
        rear = count;
        queue = larger;
    }
    public T dequeue() throws EmptyCollectionException {
        if (isEmpty())
            throw new EmptyCollectionException("queue");

        T result = queue[front];
        queue[rear] = null;
        front = (front + 1) % queue.length;

        count--;

        return result;
    }

    public T first() throws EmptyCollectionException {
        if (isEmpty())
            throw new EmptyCollectionException("queue");

        return queue[front];
    }

    public boolean isEmpty() {
        if (size() == 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T1> T1[] toArray(T1[] t1s) {
        return null;
    }

    public int size() {
        return count;
    }

    public String toString()//输出String型duilie中数据
    {
        String last = "";
        int temp = front;
        for (int a = 0; a < size(); a++) {
            last += queue[temp] + " ";
            temp = (temp + 1) % queue.length;
        }
        return last;
    }

    @Override
    public boolean add(T t) {
        return false;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends T> collection) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public boolean offer(T t) {
        return false;
    }

    @Override
    public T remove() {
        return null;
    }

    @Override
    public T poll() {
        return null;
    }

    @Override
    public T element() {
        return null;
    }

    @Override
    public T peek() {
        return null;
    }
}
