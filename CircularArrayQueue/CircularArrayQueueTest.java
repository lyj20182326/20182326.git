package javafoundations;

public class CircularArrayQueueTest {
    public static void main(String[] args) {
        CircularArrayQueue<Integer> num = new CircularArrayQueue<>();
        num.enqueue(2);
        num.enqueue(0);
        num.enqueue(1);
        num.enqueue(8);
        num.enqueue(2);
        num.enqueue(3);
        num.enqueue(3);
        num.enqueue(0);
        num.enqueue(1);
        num.enqueue(2);
        num.enqueue(3);
        System.out.println(num.size());
        System.out.println(num.dequeue());
        System.out.println(num.toString());
        System.out.println(num.first());
    }
}
