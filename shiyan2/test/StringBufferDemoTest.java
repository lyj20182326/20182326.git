import junit.framework.TestCase;
import org.junit.Test;
public class StringBufferDemoTest extends TestCase {
    StringBuffer a = new StringBuffer("StringBuffer");
    StringBuffer b = new StringBuffer("StringBufferStringBuffer");
    StringBuffer c = new StringBuffer("StringBufferStringBufferStringBuffer");
    @Test
    public void testcharAt() throws Exception {
        assertEquals('S',a.charAt(0));
        assertEquals('g',a.charAt(5));
        assertEquals('r',a.charAt(11));
    }
    @Test
    public void tesycapacity() throws Exception{
        assertEquals(28,a.capacity());
        assertEquals(40,b.capacity());
        assertEquals(52,c.capacity());
    }
    @Test
    public void testindexOf() throws Exception{
        assertEquals(0,a.indexOf("Str"));
        assertEquals(5,a.indexOf("gBu"));
        assertEquals(9,a.indexOf("fer"));
    }

}