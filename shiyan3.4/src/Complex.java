public class Complex {
    private double a, b;
    public Complex(double a1, double b1){
        a = a1;
        b = b1;
    }
    public double getRealPart(){
        return a;
    }
    public double getImagePart(){
        return b;
    }
    public Complex ComplexAdd(Complex e){
        double c = e.getRealPart();
        double d = + e.getImagePart();
        double R = a + c;
        double I = b + d;
        return new Complex(R,I);
    }
    public Complex ComplexSub(Complex e){
        double c = e.getRealPart();
        double d = + e.getImagePart();
        double R = a - c;
        double I = b - d;
        return new Complex(R,I);
    }
    public Complex ComplexMulti(Complex e){
        double c = e.getRealPart();
        double d = + e.getImagePart();
        double R = a * c - b * d;
        double I = b * c + a * d;
        return new Complex(R,I);
    }
    public Complex ComplexDiv(Complex e){
        double c = e.getRealPart();
        double d = + e.getImagePart();
        double t = c * c + d * d ;
        double R = (a*c + b*d)/t;
        double I = (b*c - a*d)/t;
        return new Complex(R,I);
    }
    public boolean Equal(Complex e){
        if(this.equals(e))
            return true;
        else
            return false;
    }
    public String toString(){
        String s = "";
        if(b>0)
            s = a + "+"+ b +"i";
        else if(b<0)
            s = a + "" + b + "i";
        else
            s = a+"";
        return s;
    }
}