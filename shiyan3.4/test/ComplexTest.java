import junit.framework.TestCase;
import org.junit.Test;

public class ComplexTest extends TestCase {
    Complex a = new Complex(1,2);
    Complex b = new Complex(2,3);
    Complex c = new Complex(1,-2);
    Complex d = new Complex(0,0);
    Complex e = new Complex(3,-4);
    @Test
    public void testComplexAdd() throws Exception{
        assertEquals("3.0+5.0i",a.ComplexAdd(b).toString());
        assertEquals("2.0",a.ComplexAdd(c).toString());
    }

    @Test
    public void testComplexSub() throws Exception{
        assertEquals("-1.0-1.0i",a.ComplexSub(b).toString());
        assertEquals("0.0+4.0i",a.ComplexSub(c).toString());
    }

    @Test
    public void testComplexMulti() throws Exception{
        assertEquals("-4.0+7.0i",a.ComplexMulti(b).toString());
        assertEquals("5.0",a.ComplexMulti(c).toString());
        assertEquals("0.0",a.ComplexMulti(d).toString());
    }

    @Test
    public void testComplexDiv() throws Exception{
        assertEquals("-0.2+0.4i",a.ComplexDiv(e).toString());
        assertEquals("NaN",a.ComplexDiv(d).toString());
    }

    @Test
    public void testEqual() throws Exception{
        assertEquals(true,a.Equal(a));
        assertEquals(false,a.Equal(b));
    }
}