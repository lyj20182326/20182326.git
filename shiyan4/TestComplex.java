package com.company;

import java.util.Scanner;

public class TestComplex {
    public static void main(String[] args) {
        Scanner scan =new Scanner(System.in);
        System.out.println("Please input 实部R1，R2虚部I1,I2: ");
        int R1= scan.nextInt();
        int R2= scan.nextInt();
        int I1= scan.nextInt();
        int I2= scan.nextInt();
        ComplexNumber r1 = new ComplexNumber(R1,I1);
        ComplexNumber r2  = new ComplexNumber(R2,I2);
        ComplexNumber r3;
        ComplexNumber r4;
        ComplexNumber r5;
        ComplexNumber r6;
        ComplexNumber r7 ;
        System.out.println("First rational number: "+ r1);
        System.out.println("Second rational number:"+ r2);

        System.out.println("Please input the symbol:");
        String m = scan.next();
        switch(m) {
            case "+":
                r4 = r1.ComplexAdd(r2);
                System.out.println("r1 +r2:" + r4);
                break;
            case "-":
                r5 = r1.ComplexSub(r2);
                System.out.println("r1 -r2 :" + r5);
                break;
            case "*":
                r6 = r1.ComplexMulti(r2);
                System.out.println("r1 *r2 :" + r6);
                break;
            case "/":
                r7 = r1.ComplexDiv(r2);
                System.out.println("r1 /r2 :" + r7);
                break;
        }
    }
}
