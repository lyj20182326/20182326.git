class Book { //定义Book类
private String bookName; //书名
private String author; //作者
private String publisher; //出版社
private String publishDate; //出版日期
public Book(String bookName, String author, String publisher, String publishDate) { //构造方法
this.setBookName(bookName);
this.setAuthor(author);
this.setPublisher(publisher);
this.setPublishDate(publishDate);
}

public void setBookName(String bookName) {
this.bookName = bookName;
}
public String getBookName() {
return bookName;
}
public void setAuthor(String author) {
this.author = author;
}
public String getAuthor() {
return author;
}
public void setPublisher(String publisher) {
this.publisher = publisher;
}
public String getPublisher() {
return publisher;
}
public void setPublishDate(String publishDate) {
this.publishDate = publishDate;
}
public String getPublishDate() {
return publishDate;
}
public String toString() {
return "-bookname：" + this.getBookName() + "\n\t" + "-author：" + this.getAuthor() +
"\n\t" + "-press：" + this.getPublisher() + "\n\t" + "-copynightdate：" + this.getPublishDate();
}
}

class Bookshelf{
public static void main(String[] args) {
Book b1 = new Book("book1name", "author", "press", "2019-01-01");
System.out.println(b1);
}
} 
