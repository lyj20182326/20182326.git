class book{
private String bookName;
private String author;
private String publisher;
private String publishDate;
public book(String bookName,String author,String publisher,String publishDate){
	this.setBookName(bookName);
	this.setAuthor(author);
	this.setPublisher(publisher);
        this.setPublishDate(publishDate);
}
public void setBookName(String bookName){
	this.bookName = bookName;
}
public String getBookName(){
	return bookName;
}
public void setAthor(String author){
	this.author = author;
}
public String getAuthor(){
	return author;
}
public void setPublisher(String publisher){
	this.publisher = publisher;
}
public void setPublishDate(String publishDate){
	this.publishDate = publishDate;
}
public String getPublishDate(){
	return publishDate;
}
public String toString(){
	return "|--bookname:" + this.getBookName() + "\n\t" + "|--author:" + this.getAuthor() + "\n\t" + "|--press:" + this.getPublisher() + "\n\t" +"|--copynightdate:" + this.getPublishDate();
}
}
public class BookShelf{
	public static void main(String[] args){
		book b1 = new book("String","String","String","String");
		System.out.println(b1);
	}
}
