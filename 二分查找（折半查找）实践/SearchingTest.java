package com.company;

import static com.company.Searching.binarySearch;

public class SearchingTest {
    public static void main(String[] args) {
        int[] data = {5,13,19,21,37,56,64,75,80,88,92};

        int low = 0 ;
        int key = 85;
        int high = data.length-1;
        int result;

        result=binarySearch(data,low,high,key);
        System.out.println(result);
    }
}
