package com.company;

import java.util.LinkedList;
import java.util.Scanner;

public class BinaryTreeNode<T> {
    public  BinaryTreetest root;
    static int count = 0;

    public BinaryTreeNode() {
        root = null;
    }

    public static BinaryTreetest Create(String arr) {


        if ( count >= arr.length() || arr.charAt(count) == '#') {
            count++;
            return null;        }
        BinaryTreetest node = new BinaryTreetest<>(arr.charAt(count++));
        node.left = Create(arr);
        node.right = Create(arr);
        return node;
    }


    public static void levelOrderTraverse(BinaryTreetest root) {
        BinaryTreetest temp = root;
        LinkedList<BinaryTreetest> linkedList = new LinkedList<BinaryTreetest>();
        linkedList.add(temp);
        while (!linkedList.isEmpty()) {
            temp = linkedList.poll();
            System.out.println(temp.element + " ");
            if (temp.left != null) {
                linkedList.add(temp.left);
            }
            if (temp.right != null) {
                linkedList.add(temp.right);
            }
        }
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("请输入一串字符串");
        String str = scan.nextLine();

        System.out.println("下面进行创建树");
        BinaryTreetest root = Create(str);
        System.out.print("层次遍历为 ：  ");
        levelOrderTraverse(root);

    }
}

