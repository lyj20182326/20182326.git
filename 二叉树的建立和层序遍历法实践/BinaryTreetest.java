package com.company;

public class BinaryTreetest<T>
{
    protected T element;
    protected BinaryTreetest<T> left, right;

    public BinaryTreetest(T element)
    {
        this.element = element;
        left = right = null;
    }

    public BinaryTreetest(T element, BinaryTreetest left, BinaryTreetest right)
    {
        this.element=element;
        this.left=left;
        this.right=right;
    }



}