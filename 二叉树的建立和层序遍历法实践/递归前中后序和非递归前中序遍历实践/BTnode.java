package newone;

public class BTnode {
    public int key;
    public String data;
    public BTnode leftChild;
    public BTnode rightChild;
    public boolean isVisted=false;
    public BTnode() {
    }
    public BTnode(int key, String data) {
        this.key = key;
        this.data = data;
    }
    public BTnode(int key, String data, BTnode leftChild,
                  BTnode rightChild) {
        this.key = key;
        this.data = data;
        this.leftChild = leftChild;
        this.rightChild = rightChild;
    }
}
