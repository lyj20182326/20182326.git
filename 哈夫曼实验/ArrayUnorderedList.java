package javafoundation;


import java.util.Arrays;

public class ArrayUnorderedList<T> extends ArrayList<T> implements UnorderedListADT<T> {
    public ArrayUnorderedList()
    {
        super();
    }

    public ArrayUnorderedList(int initialCapacity)
    {

        super(initialCapacity);
    }

    @Override
    public void addToFront(T element)
    {
        if (size() == list.length)
        {
            expandCapacity();
        }
        int scan = 0;
        for (int shift=rear; shift > scan; shift--)
        {
            list[shift] = list[shift - 1];
        }
        list[scan] = element;
        rear++;
        modCount++;
    }

    @Override
    public void addToRear(T element) {
        if (size() == list.length)
        {
            expandCapacity();
        }
        list[rear] = element;
        rear++;
        modCount++;
    }

    @Override
    public void addAfter(T element, T target) throws ElementNotFoundException {
        if (size() == list.length)
        {
            expandCapacity();
        }
        int scan = 0;

        while (scan < rear && !target.equals(list[scan]))
        {
            scan++;
        }
        if (scan == rear)
        {
            throw new ElementNotFoundException("UnorderedList");
        }
        scan++;

        for (int shift=rear; shift > scan; shift--)
        {
            list[shift] = list[shift - 1];
        }
        list[scan] = element;
        rear++;
        modCount++;
    }

    public void expandCapacity()
    {
        list = Arrays.copyOf(list, list.length * 2);
    }
}
