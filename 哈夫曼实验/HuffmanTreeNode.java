package javafoundation;


public class HuffmanTreeNode implements Comparable<HuffmanTreeNode>{
    protected int weight;
    protected HuffmanTreeNode left;
    protected HuffmanTreeNode right;
    protected HuffmanTreeNode parent;
    protected char element;

    protected HuffmanTreeNode(int weight, char element, HuffmanTreeNode left, HuffmanTreeNode right, HuffmanTreeNode parent) {
        this.weight = weight;
        this.element = element;
        this.left = left;
        this.right = right;
        this.parent = parent;
    }


    @Override
    public int compareTo(HuffmanTreeNode o) {
        if (this.weight > o.weight)
            return 1;
        else if (this.weight < o.weight)
            return -1;
        else
            return 0;
    }
}

