package javafoundation;


import java.io.*;
import java.util.Scanner;

public class HuffmanTreeTest {
    public static void main(String[] args) throws IOException, EmptyCollectionException {
        File file = new File("G:\\Huffman\\text.txt");

        Scanner scan = new Scanner(file);
        String s = scan.nextLine();
        System.out.println(s);
        int[] array = new int[26];
        for (int i = 0; i < array.length; i++) {
            array[i] = 0;
        }
        for (int i = 0; i < s.length(); i++) {
            char x = s.charAt(i);
            array[x - 'a']++;
        }
        System.out.println("打印各字母出现频率：");
        for (int i = 0; i < array.length; i++) {
            System.out.print((char) ('a' + i) + ":" + (double) array[i] / s.length() + "\n");
        }

        HuffmanTreeNode[] huffmanTreeNodes = new HuffmanTreeNode[array.length];
        for (int i = 0; i < array.length; i++) {
            huffmanTreeNodes[i] = new HuffmanTreeNode(array[i], (char) ('a' + i), null, null, null);
        }

        HuffmanTree huffmanTree = new HuffmanTree(huffmanTreeNodes);

        System.out.println("打印各字母的编码");
        String[] codes = huffmanTree.getEncoding();
        for (int i = 0; i < codes.length; i++) {
            System.out.println((char) ('a' + i) + ":" + codes[i]);
        }

        String result = "";
        for (int i = 0; i < s.length(); i++) {
            int x = s.charAt(i) - 'a';
            result += codes[x];
        }
        System.out.println("编码结果：" + result);

        File file1 = new File("G:\\Huffman\\text.txt");
        FileWriter fileWriter = new FileWriter(file1);
        fileWriter.write(result);
        fileWriter.close();

        Scanner scan1 = new Scanner(file1);
        String s1 = scan1.nextLine();
        HuffmanTreeNode huffmanTreeNode = huffmanTree.getRoot();

        //进行解码
        String result2 = "";
        for (int i = 0; i < s1.length(); i++) {
            if (s1.charAt(i) == '0') {
                if (huffmanTreeNode.left != null) {
                    huffmanTreeNode = huffmanTreeNode.left;
                }
            } else {
                if (s1.charAt(i) == '1') {
                    if (huffmanTreeNode.right != null) {
                        huffmanTreeNode = huffmanTreeNode.right;
                    }
                }
            }
            if (huffmanTreeNode.left == null && huffmanTreeNode.right == null) {
                result2 += huffmanTreeNode.element;
                huffmanTreeNode = huffmanTree.getRoot();
            }
        }
        System.out.println("解码结果：" + result2);
        File file2 = new File("G:\\Huffman\\text.txt");
        FileWriter fileWriter1 = new FileWriter(file1);
        fileWriter1.write(result2);

        System.out.println("编码解码后原来是否一致：" + result2.equals(s));
    }
}

