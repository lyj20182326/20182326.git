package Javafoundation;

import java.util.Scanner;

public class Ungraph {
    public static void main(String[] args) {
        int[][]  dig = new int[5][5];
        int i ,j;
        Scanner scan  =  new Scanner(System.in);
        System.out.println("这个是无向图");
        for(i=0;i<5;i++){
            for(j=0;j<5;j++){

                System.out.println("请输入第"+ i + "行，第" + j + "个数字： ");
                int input = scan.nextInt();
                dig[i][j] = input;
            }
        }

        int[] a = new int[5];
        for(i=0;i<5;i++)
            a[i]=0;
        for(i=0;i<5;i++){
            for(j=0;j<5;j++){
                if(dig[i][j]!=0)
                    a[i]++;
            }
        }
        System.out.println("输出每一个顶点的度：");
        for(i=0;i<5;i++)
            System.out.println(a[i]);


    }
}
