package com.company;

public class Sorting {
    public static void selectionSort(Comparable[] data) {
        int min;
        for(int index=0;index<data.length-1;index++){
            min=index;
            for(int scan=index+1;scan<data.length;scan++)
                if(data[scan].compareTo(data[min])<0)
                    min=scan;
            swap(data,min,index);
        }
    }
    private static void swap(Comparable[] data,int index1,int index2){
        Comparable temp=data[index1];
        data[index1]=data[index2];
        data[index2]=temp;
    }
}
