package cn.edu.besti.cs1823.G2326;

public class Contact implements Comparable{
    private String firstName, lastName, id;

    public Contact (String first, String last, String ID)
    {
        firstName = first;
        lastName = last;
        id = ID;
    }


    public String toString ()
    {
        return lastName + ", " + firstName + ":  " + id;
    }

    public int compareTo (Object other)
    {
        int result;
        result = id.compareTo(((Contact)other).id);
        return result;
    }

    public static void select(Contact[] a){
        Contact temp;
        int num;
        for(int i=0;i<a.length-1;i++)
        {
            num = i;
            for(int j = i + 1;j <a.length; j++)
                if(a[j].id.compareTo(a[num].id) > 0)
                    num = j;
            if(i != num)
            {
                temp = a[i];
                a[i] = a[num];
                a[num] = temp;
            }
        }
        for(int m = 0;m <a.length; m++)
            System.out.println(a[m]);
    }
}
