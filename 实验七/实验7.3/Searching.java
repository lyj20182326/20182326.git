package Sreaching;

public class Searching<T> {
    private T[] data;
    public int mid;

    public T[] getData() {
        return data;
    }

    public void setData(T[] data) {
        this.data = data;
    }

    public static Comparable linearSearch (Comparable[] data,
                                           Comparable target)
    {
        Comparable result = null;
        int index = 0;

        while (result == null && index < data.length)
        {
            if (data[index].compareTo(target) == 0)
                result = data[index];
            index++;
        }

        return result;
    }

    public static <T extends Comparable<T>>
    boolean binarysearch (Comparable[] data, int min, int max,int mid,Comparable target)
    {
        boolean found = false;

        if(data[mid].compareTo(target) == 0)
            found = true;
        else if (data[mid].compareTo(target) > 0)
        {
            if(min<mid-1)
            {
                mid--;
                found = binarysearch(data,min,max,mid,target);
            }
        }
        else if(mid+1 <= max)
        {
            mid++;
            found = binarysearch(data,min,max,mid,target);
        }
        return found;
    }

    public static <T extends Comparable<T>>
    Comparable binarySearching (Comparable[] data, int min, int max,int mid,Comparable target)
    {
        Comparable found = null;

        if(data[mid].compareTo(target) == 0)
            found = data[mid];
        else if (data[mid].compareTo(target) > 0)
        {
            if(min < mid - 1)
            {
                mid--;
                found = binarySearching(data,min,max,mid,target);
            }
        }
        else if(mid + 1 <= max)
        {
            mid++;
            found = binarySearching(data,min,max,mid,target);
        }
        return found;
    }
}
