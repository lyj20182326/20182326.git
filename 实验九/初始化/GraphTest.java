package Javafoundation;

import java.util.Stack;

public class GraphTest {
    public static void main(String[] args) {
        graph graph = new graph();
        System.out.println("该图的邻接表为：");
        output(graph);

    }

    public static void output(graph graph){
        for (int i = 0;i < graph.verNumber; i++)
        {
            Vertex vertex = graph.Array[i];
            System.out.print(vertex.verName);

            Edge current = vertex.edgeLink;
            while (current != null)
            {
                System.out.print("——>" + current.tailName);
                current = current.broEdge;
            }
            System.out.println();
        }
    }

    public static void output(graph graph,int[] num){
        Stack<Integer> stack = new Stack<>();
        for (int i = 0;i < graph.verNumber; i++)
        {
            if(num[i] == 0)
            {
                System.out.println(i+1);

            }
        }
    }
}
