package Javafoundation;

import java.util.Scanner;

public class graph {
    int verNumber;
    int edgeNumber;
    Vertex[] Array;

    public graph() {
        Scanner scan = new Scanner(System.in);
        System.out.println("构建有向图，输入0；构建无向图，输入1： ");
        int choose = scan.nextInt();
        System.out.println("请输入节点个数和边的个数：");
        verNumber = scan.nextInt();
        Array = new Vertex[verNumber];
        edgeNumber = scan.nextInt();
        System.out.println("请输入节点的名称:");
        for (int i = 0;i < verNumber; i++)
        {
            Vertex vertex = new Vertex();
            vertex.verName = scan.next();
            vertex.edgeLink = null;
            Array[i] = vertex;
        }

        System.out.println("请输入边的信息：");
        for (int i = 0;i < edgeNumber; i++)
        {
            String Name1 = scan.next();
            String Name2 = scan.next();

            Vertex preV = getVertex(Name1);
            Vertex fol = getVertex(Name2);
            if (preV == null || fol == null)
            {
                System.out.println("无该顶点！请重新输入");
                i--;
                continue;
            }

            Edge edge = new Edge();
            edge.tailName = Name2;

            edge.broEdge = preV.edgeLink;
            preV.edgeLink = edge;

            if(choose == 1)
            {
                Edge edgeelse = new Edge();
                edgeelse.tailName = Name1;
                edgeelse.broEdge  = fol.edgeLink;
                fol.edgeLink = edgeelse;
            }

        }
    }

    public Vertex getVertex(String verName){
        for (int i = 0;i < verNumber; i++)
        {
            if (Array[i].verName.equals(verName))
                return Array[i];
        }
        return null;
    }
}
