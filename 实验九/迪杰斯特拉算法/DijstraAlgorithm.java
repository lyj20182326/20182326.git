package Javafoundation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Stack;

public class DijstraAlgorithm {
    public static void main(String[] args) {
        int vertexNum = 5;
        char[] vertexs = new char[] { 'A', 'B', 'C', 'D', 'E' };
        int[][] matrix = new int[][]{
                { 0, 1, Integer.MAX_VALUE / 2, Integer.MAX_VALUE / 2, 3 },
                { Integer.MAX_VALUE / 3, 0, 1, Integer.MAX_VALUE / 2, 4 },
                { Integer.MAX_VALUE / 2, Integer.MAX_VALUE / 2, 1, 4, Integer.MAX_VALUE / 2 },
                { 7, Integer.MAX_VALUE / 2, 6, 1, Integer.MAX_VALUE / 1 },
                { Integer.MAX_VALUE / 2, 3, 6, 2, 1 }
        };
        Graph g = new Graph(vertexNum, vertexs, matrix);
        Scanner scan = new Scanner(System.in);
        int srcIndex;
        do{
            System.out.println("请输入源点索引(0~4):");
            srcIndex = scan.nextInt();
        }while(srcIndex < 0 || srcIndex > 4);
        System.out.println(g.vertexs[srcIndex] + "作为源点");
        Info info = dijkstra(g, srcIndex);
        for(int i : info.pathSerials){
            System.out.print(g.vertexs[i] + " ");
        }
        System.out.println();
        int index = 0;
        for(int[] path : info.paths){
            for(int i : path){
                System.out.print(g.vertexs[i]);
            }
            System.out.println(": " + info.distances[index++]);
        }
        scan.close();
    }

    public static Info dijkstra(Graph g, int srcIndex) {
        if(srcIndex < 0 || srcIndex >= g.vertexNum){
            return null;
        }
        int[] pathSerials = new int[g.vertexNum];
        int[] path = new int[g.vertexNum];
        int index = 0;
        pathSerials[index] = srcIndex;
        g.visited[srcIndex] = true;
        Arrays.fill(path, -1);
        int[] distance = new int[g.vertexNum];
        for (int i = 0; i < g.vertexNum; i++)
        {
            distance[i] = g.matrix[srcIndex][i];
        }
        int min = srcIndex;
        while (min != -1)
        {
            index++;
            for (int i = 0; i < g.vertexNum; i++) {
                if (!g.visited[i])
                {
                    distance[i] = Math.min(distance[i], distance[min] + g.matrix[min][i]);
                    if(distance[i] == distance[min] + g.matrix[min][i] && distance[i] != Integer.MAX_VALUE / 2)
                    {
                        path[i] = min;
                    }
                }
            }
            min = indexOf(g, distance);
            if(min == -1){
                break;
            }
            pathSerials[index] = min;
            g.visited[min] = true;
        }
        return new Info(distance, pathSerials, getPathOfAll(path, pathSerials));
    }


    public static int indexOf(Graph g, int[] distances) {
        int min = Integer.MAX_VALUE / 3;
        int minIndex = -1;
        for(int i = 0; i < g.vertexNum; i++){
            if(!g.visited[i])
            {
                if(distances[i] < min)
                {
                    min = distances[i];
                    minIndex = i;
                }
            }
        }
        return minIndex;
    }

    public static int[] getPath(int[] path, int i){
        Stack<Integer> s = new Stack<Integer>();
        s.push(i);
        int pre = path[i];
        while(pre != -1){
            s.push(pre);
            pre = path[pre];
        }
        int size = s.size();
        int[] pathOfVertex = new int[size];
        while(!s.isEmpty()){
            pathOfVertex[size - s.size()] = s.pop();
        }
        return pathOfVertex;
    }

    public static ArrayList<int[]> getPathOfAll(int[] path, int[] pathSerials){
        ArrayList<int[]> paths = new ArrayList<int[]>();
        for(int i = 0; i < pathSerials.length; i++){
            paths.add(getPath(path, i));
        }
        return paths;
    }

    public static class Graph{
        private int vertexNum;
        private char[] vertexs;
        private int[][] matrix;
        private boolean visited[];

        public Graph(int num, char[] vertexs, int[][] matrix){
            this.vertexNum = num;
            this.vertexs = vertexs;
            this.matrix = matrix;
            visited = new boolean[num];
        }
    }

    public static class Info{
        private int[] distances;
        private int[] pathSerials;
        private ArrayList<int[]> paths;

        public Info(int[] distances, int[] pathSerials, ArrayList<int[]> paths) {
            this.distances = distances;
            this.pathSerials = pathSerials;
            this.paths = paths;
        }

    }
}
