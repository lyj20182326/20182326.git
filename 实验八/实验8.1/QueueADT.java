package javafoundations;


public interface QueueADT<T>
{

   public T dequeue() throws EmptyCollectionException;


   public T first() throws EmptyCollectionException;
   

   public boolean isEmpty();


   public int size();


   public String toString();
}
