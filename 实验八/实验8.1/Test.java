package javafoundations;

public class Test {
    public static void main(String[] args) {
        LinkedBinaryTree<String> current = new LinkedBinaryTree<String>("A");
        BTNode<String> a = current.root;
        a.left = new LinkedBinaryTree<String>("B").root;
        a.right = new LinkedBinaryTree<String>("C").root;
        (a.left).left = new LinkedBinaryTree<String>("D").root;
        (a.left).right = new LinkedBinaryTree<String>("E").root;
        (a.right).left = new LinkedBinaryTree<String>("F").root;
        (a.right).right = new LinkedBinaryTree<String>("G").root;
        System.out.println("是否为空？");
        System.out.println(current.isEmpty());

        ArrayIterator<String>Iterator = (ArrayIterator<String>) current.preorder();
        System.out.println("先序遍历是：");
        for(String i :Iterator){
            System.out.println(i);
        }
        ArrayIterator<String>Iterator2 = (ArrayIterator<String>) current.postorder();
        System.out.println("后序遍历是：");
        for(String i :Iterator2){
            System.out.println(i);
        }
        ArrayIterator<String>Iterator3 = (ArrayIterator<String>) current.inorder();
        System.out.println("中序遍历是：");
        for(String i :Iterator3)
        {
            System.out.println(i);
        }
    }
}