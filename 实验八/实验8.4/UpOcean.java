import java.util.Scanner;
import java.util.Stack;

public class UpOcean {
    static Stack<Character> op = new Stack<>();

    public static float data(char op, float f1, float f2){
        if(op == '+') return f2 + f1;
        else if(op == '-') return f2 - f1;
        else if(op  == '*') return f2 * f1;
        else if(op == '/') return f2 / f1;
        else return Float.valueOf(-0);
    }


    public static float calrp(String rp){
        Stack<Float> v = new Stack<>();
        char[] arr = rp.toCharArray();
        int len = arr.length;
        for(int i = 0; i < len; i++){
            Character ch = arr[i];
            if(ch >= '0' && ch <= '9') v.push(Float.valueOf(ch - '0'));
            else v.push(data(ch, v.pop(), v.pop()));
        }
        return v.pop();
    }


    public static String getrp(String s){
        char[] arr = s.toCharArray();
        int len = arr.length;
        String out ="";

        for(int i = 0; i < len; i++){
            char ch = arr[i];
            if(ch == ' ') continue;

            if(ch >= '0' && ch <= '9') {
                out+=ch;
                continue;
            }

            if(ch == '(') op.push(ch);

            if(ch == '+' || ch == '-'){
                while(!op.empty() && (op.peek() != '('))
                    out+=op.pop();
                op.push(ch);
                continue;
            }


            if(ch == '*' || ch == '/'){
                while(!op.empty() && (op.peek() == '*' || op.peek() == '/'))
                    out+=op.pop();
                op.push(ch);
                continue;
            }

            if(ch == ')'){
                while(!op.empty() && op.peek() != '(')
                    out += op.pop();
                op.pop();
                continue;
            }
        }
        while(!op.empty()) out += op.pop();
        return out;
    }

    public static void main(String[] args){
        System.out.println("输入中缀表达式：");
        Scanner scan=new Scanner(System.in);
        String exp=scan.nextLine();
        System.out.println("则后缀表达式为：");
        System.out.println(getrp(exp));
        System.out.println("结果为：");
        System.out.println(calrp(getrp(exp)));
    }

}
