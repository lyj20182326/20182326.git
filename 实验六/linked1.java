package linked;

import java.util.Scanner;

public class linked1 {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("请输入一串学号日期时间的整数（eg：20， 17，23，1， 20， 18，10，1，16，23，49）：  ");
        String str = scan.nextLine();
        String[] arr = str.split(",");
        System.out.println(Arrays.toString(arr));
        NumNode head = new NumNode(Integer.valueOf(arr[0]));
        int[] intss=new int[20];
        intss=StringToInt(arr);

        for(int i=1;i<10;i++){
            NumNode x = new NumNode(intss[i]);
            Addtrail(head,x);
        }

        linked1.PrintLink(head);
        int nzpn=linked1.length(head);
        System.out.println("\n"+"链表的个数为：  "+nzpn);

    }
    public static int[] StringToInt(String[] arrs){

        int[] ints = new int[arrs.length];

        for(int i=0;i<arrs.length;i++){

            ints[i] = Integer.parseInt(arrs[i]);

        }

        return ints;

    }

    private static class NumNode
    {
        protected NumNode next = null;
        protected int num;
        public NumNode(int num)
        {
            this.num = num;
            next = null;
        }
    }
    public static void Addtrail(NumNode head, NumNode node)
    {
        //尾插法
        if ( head == null)
            head = node;
        else {
            NumNode temp = head;
            while (temp.next != null) {
                temp = temp.next;
            }
            temp.next = node;
        }
    }
    public static void PrintLink(NumNode Head) {
        NumNode node = Head;
        while (node != null) {
            System.out.print(" " + node.num);
            node = node.next;
        }
    }
    public static int length(NumNode head){
        NumNode temp = head;
        int nliuyingjie=1;
        while(temp.next!=null){
            temp=temp.next;
            nliuyingjie++;
        }
        return nliuyingjie;
    }
}
