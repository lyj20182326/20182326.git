package linked;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;
import java.util.StringTokenizer;

public class linked3 {
    public static void main(String[] args) throws IOException {

        //从文件读取数字
        File file = new File("G:/File/file.txt");
        FileReader fileReader = new FileReader(file);
        BufferedReader R =new BufferedReader(fileReader);
        String p ="";
        p= R.readLine();
        fileReader.close();

        StringTokenizer tokenizer = new StringTokenizer(p);
        int[] number = new int[tokenizer.countTokens()];
        int y = 0;
        while(tokenizer.hasMoreTokens()){
            number[y] = Integer.parseInt(tokenizer.nextToken());
            y++;
        }
        int m = number[0];
        NumNode m1 = new NumNode(m);
        int n = number[1];
        NumNode n1 = new NumNode(n);


        Scanner scan = new Scanner(System.in);
        System.out.println("请输入一串学号日期时间的整数（eg：20， 17，23，1， 20， 18，10，1，16，23，49）：  ");
        String str = scan.nextLine();
        String[] arr = str.split(","); // 用,分割
        System.out.println(Arrays.toString(arr)); // [0, 1, 2, 3, 4, 5]
        NumNode head = new NumNode(Integer.valueOf(arr[0]));
        int[] intss=new int[20];
        intss=StringToInt(arr);

        for(int i=1;i<10;i++){
            NumNode x = new NumNode(intss[i]);
            AddTrail(head,x);
        }

        PrintLink(head);
        int nzpn=length(head);
        System.out.println("\n"+"链表的个数为：  "+nzpn);

        System.out.println("在第五位插入文件中第一个数");
        AddShu(5,m1,head);
        PrintLink(head);
        System.out.println("\n"+"链表的个数为：  "+length(head));
        System.out.println("在第0位插入第二个数");
        head=AddHead(head,n1);
        PrintLink(head);
        System.out.println("\n"+"链表的个数为：  "+length(head));
        System.out.println("从链表中删除刚才的数字1");
        Delete(head,m1);
        PrintLink(head);
        System.out.println("\n"+"链表的个数为：  "+length(head));
        Select(head);

    }

    public static int[] StringToInt(String[] arrs){

        int[] ints = new int[arrs.length];

        for(int i=0;i<arrs.length;i++){

            ints[i] = Integer.parseInt(arrs[i]);

        }

        return ints;

    }

    private static class NumNode
    {
        protected NumNode next = null;
        protected int num;
        public NumNode(int num)
        {
            this.num = num;
            next = null;
        }
    }



    public static void AddTrail(NumNode head, NumNode node)
    {
        //尾插法
        if ( head == null)
            head = node;
        else {
            NumNode temp = head;
            while (temp.next != null) {
                temp = temp.next;
            }
            temp.next = node;
        }
    }
        public static void PrintLink(NumNode Head) {
        NumNode node = Head;
        while (node != null) {
            System.out.print(" " + node.num);
            node = node.next;
        }
    }
    public static int length(NumNode head){
        NumNode temp = head;
        int nliuyingjie=1;
        while(temp.next!=null){
            temp=temp.next;
            nliuyingjie++;
        }
        return nliuyingjie;
    }
    public static NumNode Delete(NumNode head, NumNode node) {

        NumNode current = head, prev = head;
        if(current.num==node.num){
            head=current.next;}
        while (current != null) {
            if (current.num != node.num) {
                prev = current;
                current = current.next;
            }
            else {
                break;
            }
        }
        prev.next = current.next;
        if (current.num != node.num)
            System.out.println("找不到节点，删除失败。");
        return head;
    }
    public static NumNode AddHead(NumNode head,NumNode node)
    {
        //头插法
        node.next = head;
        head = node;
        return head;
    }

    public static void AddShu(int x, NumNode element,NumNode head)
    {
        NumNode temp = head;
        if (x == 0)
        {
            element.next = head;
            head = element;
        }
        else {
            for (int y = 1; y < x - 1; y++) {
                temp = temp.next;
            }
            element.next = temp.next;
            temp.next = element;
        }
    }
    public static void  Select(NumNode head)
    {
        NumNode current = head;
        int temp;

        while (current != null)
        {
            NumNode numNode = current.next;
            while (numNode != null)
            {
                if (numNode.num < current.num)
                {
                    temp = current.num;
                    current.num = numNode.num;
                    numNode.num = temp;
                }
                numNode = numNode.next;
            }
            current = current.next;
            PrintLink(head);
            System.out.println("\n"+"链表的个数为：  "+length(head));

        }
    }
}
