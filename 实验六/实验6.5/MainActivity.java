package com.example.jllb;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.Arrays;

import static com.example.jllb.Size.AddTrail;
import static com.example.jllb.Size.StringToInt;
import static com.example.jllb.Size.length;

public class MainActivity extends AppCompatActivity {

    private EditText et_username;
    private Button btn_send;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        et_username = (EditText) findViewById(R.id.et_username);
        btn_send = (Button) findViewById(R.id.btn_send);

        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,PrintActivity.class);
                String str= et_username.getText().toString().trim();
                String[] arr = str.split(",");
                System.out.println(Arrays.toString(arr));
                Size.NumNode head = new Size.NumNode(Integer.valueOf(arr[0]));
                int[] intss=new int[20];
                intss=StringToInt(arr);

                for(int i=1;i<10;i++){
                    Size.NumNode x = new Size.NumNode(intss[i]);
                    AddTrail(head,x);
                }
                int nlyj=length(head);
                System.out.println("\n"+"链表的个数为：  "+nlyj);
                intent.putExtra("nlyj",nlyj);
                intent.putExtra("shuzu", Size.PrintLink(head));

                startActivity(intent);
            }
        });
    }
}
