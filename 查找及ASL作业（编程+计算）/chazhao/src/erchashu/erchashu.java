package erchashu;

public class erchashu {
    private leaf root;
    public erchashu(){
        root=null;
    }
    public void insert(int data){
        leaf temp=new leaf(data);
        if(root==null)
            root=temp;
        else{
            leaf now=root;
            leaf pre;
            while(true){
                pre=now;
                if(data<now.data){
                    now=now.low;
                    if(now==null){
                        pre.low=temp;
                        break;
                    }
                }
                else{
                    now=now.high;
                    if(now==null){
                        pre.high=temp;
                        break;
                    }
                }
            }
        }
    }
    public void theTree(int[] data){
        for(int i=0;i<data.length;i++){
            insert(data[i]);
        }
    }
    public boolean searching(int a){
        leaf now=root;
        while(now!=null){
            if(now.data==a){
                return true;
            }
            else if(now.data>a){
                now=now.low;
            }
            else{
                now=now.high;
            }
        }
        return false;
    }
}
