package erfen;

public class erfen {
    public static int binarySearch(int[] arr, int key) {
        if (null == arr || arr.length <= 0) {
            return -1;
        }
        int low= 0;
        int high= arr.length - 1;
        while (low <= high) {
            int mid = (low + high) / 2;
            if (key == arr[mid]) {
                return mid;
            }
            if (key < arr[mid]) {
                high = mid - 1;
            } else {
                low = mid + 1;
            }
        }
        return -1;
    }
}
