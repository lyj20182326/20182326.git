package stack;

import java.util.Stack;

public class LinkedStack<T> implements StackADT<T> {
    private int count;
    private LinnearNode<T> top;//ջ

    //
    public LinkedStack(){
        count=0;
        top=null;
    }

    @Override
    public void push(T element) {
        LinnearNode<T> temp=new LinnearNode<T>(element);
        temp.setNext(top);
        top = temp;
        count++;
    }

    @Override
    public T pop() throws EmptyCollectionException {
        if (isEmpty())
            throw new EmptyCollectionException("Stack");
        T result = top.getElement();
        top=top.getNext();
        count--;

        return result;
    }

    @Override
    public T peek() {
        if (isEmpty())
            throw new EmptyCollectionException("Stack");
        T result = top.getElement();

        return result;
    }

    @Override
    public boolean isEmpty() {
        boolean p ;
        if (size()==0){
            p=true;
        }
        else
        {
            p=false;
        }
        return p;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public String toString(){
        LinnearNode<T> node ;
        String a = "";
        node = top;

        while (node != null) {
            a += node.getElement().toString()+ " ";
            node = node.getNext();
        }
        return a;
    }
}
