package stack;

public class LinkedStacktest {
    public static void main(String[] args) {
        LinkedStack a =  new LinkedStack();


        a.push(1);
        a.push(2);
        a.push(3);
        a.push(4);
        a.push(5);

        System.out.println("输出栈顶的数： "+ a.pop());
        System.out.println("当前栈顶的数为： " + a.peek());
        System.out.println("是否为空？" + a.isEmpty());
        a.push(11);
        System.out.println("栈中有几个数："+ a.size());
        System.out.println("栈中为："+ a.toString());
        a.push(12);
        System.out.println("栈中为："+ a.toString());


    }
}
