package com.company;

import java.util.Scanner;

public class Postfix {
    public static void main(String[] args) {
        String expression;
        int result;

        Scanner in = new Scanner(System.in);
        PostfixEvaluator evaluator = new PostfixEvaluator();
        System.out.println("e.g. 5 4 + 3 2 1 - + *,please enter a space between each token");
        expression = in.nextLine();

        System.out.println("The expression equals " + expression);
        result = evaluator.evaluate(expression);
        System.out.println("result：" + result);
    }
}
