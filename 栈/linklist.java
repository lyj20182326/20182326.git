package linked;

import java.util.Scanner;

public class linklist {
    public static void main(String[] args) {
        Scanner scanner1 = new Scanner(System.in);
        Scanner scanner2 = new Scanner(System.in);
        System.out.println("Please enter two number:");

        int a = scanner1.nextInt();
        Integer Head = new Integer(a);
        System.out.println("Do you want to continue?(y/n)");
        String b = scanner2.nextLine();

        if (b.equalsIgnoreCase("y")) {
            do {
                int c = scanner1.nextInt();
                System.out.println("Continue?(y/n)?");
                b = scanner2.nextLine();
                Integer student = new Integer (c);
                insertNumber3 (Head,student );//尾插法
            }
            while (b.equalsIgnoreCase("y") );
        }
        System.out.println("Your numbers is: ");
        PrintLinked(Head);

        //插入数字
        System.out.println("");
        System.out.println("Which number do you want to insert?");
        Scanner d = new Scanner(System.in);
        int num = d.nextInt();
        Integer  temp= new Integer (num);
        System.out.println("Where do you want to insert?");
        Scanner e =new Scanner(System.in);
        int f =e.nextInt();
        Integer  temp1 =Head;
        for (int l =1;l<f;l++){
            temp1=temp1.next;
        }
        insertNumber2(Head,temp1,temp1.next.next,temp);
        System.out.println("The list is: ");
        PrintLinked(Head);
        System.out.println("Which number do you want to delete?");
        Scanner g = new Scanner(System.in);
        int nn = g.nextInt();
        Integer  g1 = new Integer (nn);
        delect(Head,g1);
        System.out.println("The deleted list is :");
        PrintLinked(Head);
        System.out.println("The right list is :");
        selectionSort(Head);
        PrintLinked(Head);
    }


    public static void PrintLinked(Integer  Head) {
        Integer  node = Head;
        while (node != null) {
            System.out.print(node.number+" ");
            node = node.next;
        }
    }//打印链表


    public static void insertNumber2(Integer  Head, Integer node1, Integer  node2, Integer  node3) {
        Integer  point = Head;
        while (point.number != node1.number && point != null) {
            point = point.next;
        }
        if (point.number == node1.number) {
            node3.next = point.next;
            point.next = node3;
        }
    }//中间插入


    public static void insertNumber3(Integer  Head, Integer  node1) {//尾插法
          Integer  temp = Head;
        while (temp.next != null) {
            temp = temp.next;
        }
        temp.next = node1;
    }//尾插法



    public static void delect(Integer Head, Integer  node) {
        Integer  prenode = Head, curnode = Head;
        while (prenode != null) {
            if (curnode.number != node.number) {
                prenode = curnode;
                curnode = curnode.next;
            } else {
                break;
            }
        }
        prenode.next = curnode.next;
    }//删除



    public static Integer  selectionSort(Integer  Head) {
        int temp;
        Integer  curNode = Head;
        while (curNode != null) {
            Integer  neNode = curNode.next;
            while (neNode != null) {
                if (neNode.number > curNode.number) {
                    temp = neNode.number;
                    neNode.number = curNode.number;
                    curNode.number = temp;
                }
                neNode = neNode.next;
            }
            curNode = curNode.next;
        }
        return Head;
    }//选择排序
}
