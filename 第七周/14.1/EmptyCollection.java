package javafoundations;

public class EmptyCollection extends RuntimeException{
    public EmptyCollection(String collection)
    {
        super("The " + collection + " is empty.");
    }
}
