package Javafoundations;

import org.hamcrest.internal.ArrayIterator;

public class BinaryTreeNode<T> {
    protected T element;
    protected BinaryTreeNode<T> left, right;
    public BinaryTreeNode (T element)
    {
        this.element = element;
        left = right = null;
    }
    public T getElement()
    {
        return element;
    }
    public void setElement (T element)
    {
        this.element = element;
    }
    public BinaryTreeNode<T> getLeft()
    {
        return left;
    }

    public void setLeft (BinaryTreeNode<T> left)
    {
        this.left = left;
    }

    public BinaryTreeNode<T> getRight()
    {
        return right;
    }

    public void setRight (BinaryTreeNode<T> right)
    {
        this.right = right;
    }


    public BinaryTreeNode<T> find (T target)
    {
        BinaryTreeNode<T> result = null;

        if (element.equals(target))
            result = this;
        else
        {
            if (left != null)
                result = left.find(target);
            if (result == null && right != null)
                result = right.find(target);
        }

        return result;
    }

    public int count()
    {
        int result = 1;

        if (left != null)
            result += left.count();

        if (right != null)
            result += right.count();

        return result;
    }
    public void inorder ( ArrayIterator<T> iter)
    {
        if (left != null)
            left.inorder (iter);

        iter.add (element);

        if (right != null)
            right.inorder (iter);
    }
    public void preorder ( ArrayIterator<T> iter) {
        iter.add(element);

        if(left!=null)
            left.preorder(iter);

        if (right != null)
            right.preorder(iter);
    }

    public void postorder ( ArrayIterator<T> iter) {
        if(left != null)
            left.postorder(iter);

        if(right != null)
            right.postorder(iter);

        iter.add(element);
    }
}

