package com.company;

import java.util.Random;


public class LinkedBinarySearchTreeTest {
    public static void main(String[] args) throws Exception {
        LinkedBinarySearchTree<Integer> current = new LinkedBinarySearchTree<>(50);
        BTNode<Integer> a = current.root;
        a.left = new LinkedBinarySearchTree<Integer>(40).root;
        a.right = new  LinkedBinarySearchTree<Integer>(70).root;
        (a.left).left = new  LinkedBinarySearchTree<Integer>(20).root;
        (a.left).right = new  LinkedBinarySearchTree<Integer>(30).root;
        (a.right).left = new  LinkedBinarySearchTree<Integer>(60).root;
        (a.right).right = new  LinkedBinarySearchTree<Integer>(80).root;
        ArrayIterator<Integer>Iterator = (ArrayIterator<Integer>) current.preorder();
        System.out.println("先序遍历：");
        for(int i :Iterator){
            System.out.println(i);
        }
        System.out.println("查找最小值："+ current.findMin());
        System.out.println("查找最大值："+ current.findMax());
    }
}