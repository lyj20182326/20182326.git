package SearchPlayerList;

public class Contact implements Comparable{
    private String firstName,lastName,ID;
    public Contact(String first,String last,String id)
    {
        firstName=first;
        lastName=last;
        ID=id;
    }
    public String toString()
    {
        return lastName+","+firstName+":"+ID;
    }
    public int compareTo(Object other)
    {
        int result;
        if(lastName.equals(((Contact)other).lastName))
            result=firstName.compareTo(((Contact)other).firstName);
        else
            result=lastName.compareTo(((Contact)other).lastName);
        return result;
    }
}