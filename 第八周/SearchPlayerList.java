package SearchPlayerList;

import java.util.concurrent.Callable;
import java.util.zip.CheckedInputStream;

public class SearchPlayerList {
    public static void main(String[] args) {
        Contact[] players=new Contact[7];

        players[0]=new Contact("z","pn","20182301");
        players[1]=new Contact("s","jw","20182302");
        players[2]=new Contact("z","dy","20182303");
        players[3]=new Contact("y","y","20182325");
        players[4]=new Contact("l","yj","20182326");
        players[5]=new Contact("z","th","20182327");
        players[6]=new Contact("z","jh","20182328");

        Contact target=new Contact("l","yj","20182326");
        Contact found=(Contact)Searching.LinearSearch(players,target);
        if(found==null)
            System.out.println("Players was not found.");
        else
            System.out.println("Found: "+found);
    }
}
