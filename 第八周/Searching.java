package SearchPlayerList;

public class Searching {
    public static Comparable LinearSearch(Comparable[] data,Comparable target)
    {
        Comparable result=null;
        int index=0;

        while(result==null&&index<data.length)
        {
            if(data[index].compareTo(target)==0)
                result=data[index];
            index++;
        }
        return result;
    }

    public static Comparable binarySearch(Comparable[] data,Comparable target)
    {
        Comparable result=null;
        int first=0,last=data.length,mid;

        while(result==null&&first<=last)
        {
            mid=(first+last)/2;
            if(data[mid].compareTo(target)==0)
                result=data[mid];
            else
            if(data[mid].compareTo(target)>0)
                last=mid-1;
            else
                first=mid+1;
        }
        return result;
    }

    public static<T>
    boolean linearSearch(T[] data,int min,int max,T target)
    {
        int index=min;
        boolean found=false;

        while(!found&&index<=max)
        {
            found=data[index].equals(target);
            index++;
        }
        return found;
    }
}
