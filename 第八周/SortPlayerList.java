package SearchPlayerList;

public class SortPlayerList {
    public static void main(String[] args) {
        Contact[] players = new Contact[7];

        players[0] = new Contact("z", "pn", "20182301");
        players[1] = new Contact("s", "jw", "20182302");
        players[2] = new Contact("z", "dy", "20182303");
        players[3] = new Contact("y", "y", "20182325");
        players[4] = new Contact("l", "yj", "20182326");
        players[5] = new Contact("z", "th", "20182327");
        players[6] = new Contact("z", "jh", "20182328");

        Sorting.selectionSort(players);
        for (Comparable player : players)
            System.out.println(player);
    }
}
