package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.StringTokenizer;

import static com.example.myapplication.Size.AddHead;
import static com.example.myapplication.Size.AddShu;
import static com.example.myapplication.Size.AddTrail;
import static com.example.myapplication.Size.Delete;
import static com.example.myapplication.Size.PrintLink;
import static com.example.myapplication.Size.Select;
import static com.example.myapplication.Size.StringToInt;
import static com.example.myapplication.Size.length;

public class MainActivity extends AppCompatActivity {

    private TextView et_username;
    private Button btn_send;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        et_username = (TextView) findViewById(R.id.et_username);
        final String str = String.format(String.valueOf(et_username));
        btn_send = (Button) findViewById(R.id.btn_send);

        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this,PrintActivity.class);

                StringTokenizer tokenizer = new StringTokenizer(str);
                int[] number = new int[tokenizer.countTokens()];
                int i = 0;
                while(tokenizer.hasMoreTokens()){
                    number[i] = Integer.parseInt(tokenizer.nextToken());
                    i++;
                }

                LinkedList list2 = new LinkedList();
                for(int m = 0; m<number.length; m++){
                    list2.add(number[m]);
                }
                intent.putExtra("str",list2);

                //在第四位插入5
                intent.putExtra("str1",list2);
                list2.add(3,5);
                //删除第五位
                list2.remove(4);
                intent.putExtra("str2",list2);


                ArrayList list1 = new ArrayList();
                for(int m = 0; m<number.length; m++){
                    list1.add(number[m]);
                }
                //修改第四位为6
                list1.set(3,6);
                intent.putExtra("str3",list1);
                //排序
                Collections.sort(list1);
                intent.putExtra("str4",list1);
                Collections.reverse(list1);
                intent.putExtra("str5",list1);


                startActivity(intent);
            }
        });
    }
}
